package com.gpdl.backoffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackofficeApplication {

	public static void main(String[] args) {
		System.out.println("Listing Environment Variables");
		System.out.println("GPDL_ENV:" + System.getenv("GPDL_ENV"));

		SpringApplication.run(BackofficeApplication.class, args);
	}

}
