package com.gpdl.backoffice.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VendorConroller {

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {
        return("The Vendor Controller Service is up and Running");
    }

    @RequestMapping(value = "/kill", method = RequestMethod.GET)
    public String kill() {
        System.exit(1);
        return("The Vendor Controller Service is up and Running");
    }

}
